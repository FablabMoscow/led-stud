#include <FastLED.h>

#define LED_PIN     13  // Номер пина (входа) на плате. это пин, через который Ардуино передает кманды контроллерам светодиодной ленты
#define NUM_LEDS    18  // Количество светодиодов в лента вашего проекта
#define LED_TYPE    WS2812B // Тип ленты, которая используется. тип написан на самой ленте.
#define LED_ORDER   GRB // Порядок цветов - RGB или GRB

CRGB leds[NUM_LEDS]; 

void setup()
{
    delay(3000); // Power-up safety delay
    FastLED.addLeds<LED_TYPE, LED_PIN, LED_ORDER>(leds, NUM_LEDS); // Задаем массив leds как массив светодиодов
}

// Задаем функцию для выставления яркости всех светодиодов
void setAll(int val)
{
    for (int i = 0; i < NUM_LEDS; i++)
    {
        leds[i] = CHSV(55, 181, val);
    }
    FastLED.show(); // Отправляем новые значения на светодиоды
}

void loop()
{
    // Плавно включаем светодиоды
    for (int i = 0; i < 255; ++i)
    {
        setAll(i); // Переменная i задает яркость в функции setAll
        delay(10);
    }

    // Плавно выключаем светодиоды
    for (int i = 255; i > 0; --i)
    {
        setAll(i);
        delay(10);
    }
}